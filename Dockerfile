FROM registry.gitlab.com/emulation-as-a-service/emulators/emulators-base

LABEL "EAAS_EMULATOR_TYPE"="contralto"
LABEL "EAAS_EMULATOR_VERSION"="git+eaas-01032019"


run apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
run  apt install apt-transport-https ca-certificates -y
run echo "deb https://download.mono-project.com/repo/ubuntu stable-xenial main" | tee /etc/apt/sources.list.d/mono-official-stable.list

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
git \
build-essential \
mono-complete libsdl2-2.0-0 libsdl2-dev \
nuget tzdata 

run git clone https://github.com/livingcomputermuseum/ContrAlto.git

workdir /ContrAlto
run nuget restore Contralto.sln
run xbuild Contralto.sln


#FROM registry.gitlab.com/emulation-as-a-service/emulators/emulators-base
#RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
#libsdl2-2.0-0 mono-runtime libmono-system-windows-forms4.0-cil

#RUN nuget install SDL2
#RUN mkdir -p /opt
#COPY --from=0 /ContrAlto/Contralto/bin/Debug /opt/ContrAlto

add metadata /metadata
